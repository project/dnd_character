<?php

namespace Drupal\dnd_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'dnd_fields_abilities' widget.
 *
 * @FieldWidget(
 *   id = "dnd_fields_abilities",
 *   module = "dnd_fields",
 *   label = @Translation("D&D Character Abilities"),
 *   field_types = {
 *     "dnd_fields_abilities"
 *   }
 * )
 */
class AbilitiesWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Set up the form element for this widget as a table.
    $element += [
      '#type' => 'table',
      '#header' => [
        $this->t('Ability'),
        $this->t('Base value'),
        $this->t('Temp value'),
      ],
      '#element_validate' => [
        [$this, 'validate'],
      ],
    ];

    $row = 0;

    // Add in the attribute textfield elements.
    foreach ([
      'str' => $this->t('Strength'),
      'dex' => $this->t('Dexterity'),
      'con' => $this->t('Constitution'),
      'int' => $this->t('Intelligence'),
      'wis' => $this->t('Wisdom'),
      'chr' => $this->t('Charisma'),
    ] as $attribute => $title) {
      $temp_value = 'temp_' . $attribute;

      $element[$attribute]['label'] = [
        '#type' => 'label',
        '#title' => $title,
      ];

      $element[$attribute][$attribute] = [
        '#type' => 'textfield',
        '#size' => 2,
        '#default_value' => $items[$delta]->$attribute,
        '#attributes' => ['class' => ['dnd-fields-abilities-entry']],
        '#field_suffix' => '<span></span>',
      ];

      $element[$attribute][$temp_value] = [
        '#type' => 'textfield',
        '#size' => 2,
        '#default_value' => $items[$delta]->$temp_value,
        '#attributes' => ['class' => ['dnd-fields-abilities-entry']],
        '#field_suffix' => '<span></span>',
      ];

      // Since Form API doesn't allow a fieldset to be required, we
      // have to require each field element individually.
      if ($element['#required']) {
        $element[$attribute]['#required'] = TRUE;
      }
    }

    $element['#attached'] = [
      // Add javascript to manage the bonus values for attributes as they're
      // entered into to the field elements.
      'library' => [
        'dnd_fields/abilities_widget',
      ],
    ];

    return $element;
  }

  /**
   * Validate the fields and convert them into a single value as text.
   */
  public function validate($element, FormStateInterface $form_state) {
    // Validate each of the textfield entries.
    $values = [];

    // Ideally, this would be an array map set up to parse these values out.
    foreach (['str', 'dex', 'con', 'int', 'wis', 'chr'] as $ability) {
      $values[$ability] = $element[$ability][$ability]['#value'];
      $values['temp_' . $ability] = $element[$ability]['temp_' . $ability]['#value'];

      if ($element['#required']) {
        // Don't accept blank values for the base ability values (fine for
        // temporary ability values, though).
        if (empty($values[$ability])) {
          $form_state->setError($element, $this->t('All base ability scores must have values'));
        }

        // Reject any value that isn't a digit, and is in the range of -4-34.
        if (!ctype_digit($values[$ability][$ability]) || $values[$ability] > 30 || $values[$ability] < 0) {
          $form_state->setError($element, $this->t('Ability scores must be a positive and realistically valid integer (between 0 and 30).'));
        }
      }
      else {
        // Reject any value that isn't a digit.
        if (!empty($values[$ability][$ability]) && !ctype_digit($values[$ability][$ability])) {
          $form_state->setError($element, $this->t('Ability scores must be a positive and realistically valid integer (suggested between 0 and 30).'));
        }
      }

      // Make sure temporary attribute values are at least a valid integer.
      if (!empty($values[$ability][$ability]) && !ctype_digit($values[$ability]['temp_' . $ability])) {
        $form_state->setError($element, $this->t('Temporary ability scores must be a reasonably valid integer, or left blank.'));
      }

      if (!isset($values[$ability][$ability])) {
        $values[$ability][$ability] = $element[$ability]['temp_' . $ability]['#value'] = NULL;
      }

    }
// print '<pre>';
// print_r($values);
// exit;
    // Set the value of the entire form element.
    $form_state->setValueForElement($element, $values);
  }
}
