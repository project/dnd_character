<?php

namespace Drupal\dnd_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'dnd_fields_abilities' field type.
 *
 * @FieldType(
 *   id = "dnd_fields_abilities",
 *   label = @Translation("Abilities"),
 *   module = "dnd_fields",
 *   category = @Translation("D&D Character"),
 *   description = @Translation("Lists a D&D Character's ability scores and modifiers."),
 *   default_widget = "dnd_fields_abilities",
 *   default_formatter = "dnd_fields_abilities"
 * )
 */
class Abilities extends FieldItemBase {

  /**
   * List of attributes and labels.
   *
   * @var array
   */
  public static $abilities = [
    'str' => 'Strength',
    'dex' => 'Dexterity',
    'con' => 'Constitution',
    'int' => 'Intelligence',
    'wis' => 'Wisdom',
    'chr' => 'Charisma',
  ];

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [];

    foreach (self::$abilities as $ability => $label) {
      $columns[$ability] = [
        'description' => $label,
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'unsigned' => FALSE,
      ];
    }

    foreach (self::$abilities as $ability => $label) {
      $columns['temp_' . $ability] = [
        'description' => 'Temporary ' . $label,
        'type' => 'char',
        'length' => '2',
      ];
    }

    return [
      'description' => 'The six attribute scores for a D&D Character.',
      'columns' => $columns,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    // Just check for one of the values since all are required.
    $str = $this->get('str')->getValue();
    return empty($str);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    foreach (self::$abilities as $key => $label) {
      $properties[$key] = DataDefinition::create('string')
        ->setLabel(t($label));
      $properties['temp_' . $key] = DataDefinition::create('string')
        ->setLabel(t('Temporary ' . $key));
    }

    return $properties;
  }

}
