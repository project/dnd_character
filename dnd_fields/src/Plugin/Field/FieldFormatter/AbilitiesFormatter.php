<?php

namespace Drupal\dnd_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the dnd_fields Abilities formatter.
 *
 * @FieldFormatter(
 *   id = "dnd_fields_abilities",
 *   label = @Translation("D&D Character Abilities"),
 *   field_types = {
 *     "dnd_fields_abilities"
 *   }
 * )
 */
class AbilitiesFormatter extends FormatterBase {

  /**
   * List of attributes and labels.
   *
   * @var array
   */
  public static $abilities = [
    'str' => 'Strength',
    'dex' => 'Dexterity',
    'con' => 'Constitution',
    'int' => 'Intelligence',
    'wis' => 'Wisdom',
    'chr' => 'Charisma',
  ];

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays D&D ability scores.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    // The $delta is for supporting multiple field cardinality. We don't expect
    // to have to worry about that here, but let's support it just in case.
    foreach ($items as $delta => $item) {
      $header = [
        $this->t('Ability'),
        $this->t('Base Score'),
        $this->t('Base Modifier'),
        $this->t('Temporary Score'),
        $this->t('Temporary Modifier'),
      ];

      $rows = [];

      foreach (self::$abilities as $ability => $label) {
        $temp_ability = 'temp_' . $ability;
        $temp_value = $item->$temp_ability;

        $rows[$ability]['label'] = $this->t($label);
        $rows[$ability]['base_score'] = $item->$ability;
        $rows[$ability]['base_modifier'] = floor(($item->$ability - 10) / 2);
        $rows[$ability]['temp_score'] = $item->$temp_ability;
        $rows[$ability]['temp_modifier'] = (is_numeric($item->$temp_ability)) ? floor(((int)$item->$temp_ability - 10) / 2) : NULL;

        $element[$delta] = [
          '#type' => 'table',
          '#header' => $header,
          '#rows' => $rows,
        ];

      }
      // exit;
    }

    return $element;
  }

}
