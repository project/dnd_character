<?php

namespace Drupal\Tests\dnd_fields\Kernel;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests the new entity API for the abilities field type.
 *
 * @group dnd_fields
 */
class AbilitiesFieldTest extends FieldKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['dnd_fields'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Create a dnd_fields Abilities field storage and field for validation.
    FieldStorageConfig::create([
      'field_name' => 'field_test',
      'entity_type' => 'entity_test',
      'type' => 'dnd_fields_abilities',
    ])->save();
    FieldConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'field_test',
      'bundle' => 'entity_test',
    ])->save();
  }

  /**
   * Tests using entity fields of the dnd_fields Abilities field type.
   */
  public function testAbilitiesField() {
    // Verify entity creation.
    $entity = EntityTest::create();

    $values = [
      'str' => rand(3, 18),
      'dex' => rand(3, 18),
      'con' => rand(3, 18),
      'int' => rand(3, 18),
      'wis' => rand(3, 18),
      'chr' => rand(3, 18),
    ];
    foreach ($values as $attribute => $value) {
      $values['temp_' . $attribute] = $value + rand(-2, 2);
    }

    $entity->field_test = $values;

    foreach ($values as $attribute => $value) {
      $entity->name->$attribute = $value;
    }

    $entity->save();

    // Verify entity has been created properly.
    $id = $entity->id();
    $entity = EntityTest::load($id);
    $this->assertTrue($entity->field_test instanceof FieldItemListInterface, 'Field implements interface.');
    $this->assertTrue($entity->field_test[0] instanceof FieldItemInterface, 'Field item implements interface.');
    foreach ($values as $attribute => $value) {
      $this->assertEqual($entity->field_test->$attribute, $value);
      $this->assertEqual($entity->field_test[0]->$attribute, $value);
    }

    // Verify changing the field value.
    $new_values = [
      'str' => rand(3, 18),
      'dex' => rand(3, 18),
      'con' => rand(3, 18),
      'int' => rand(3, 18),
      'wis' => rand(3, 18),
      'chr' => rand(3, 18),
    ];
    foreach ($new_values as $attribute => $value) {
      $values['temp_' . $attribute] = $value + rand(-2, 2);
    }

    $entity->field_test = $new_values;
    foreach ($new_values as $new_attribute => $new_value) {
      $this->assertEqual($entity->field_test->$new_attribute, $new_value);
      $this->assertEqual($entity->field_test[0]->$new_attribute, $new_value);
    }

    // Read changed entity and assert changed values.
    $entity->save();
    $entity = EntityTest::load($id);
    foreach ($new_values as $new_attribute => $new_value) {
      $this->assertEqual($entity->field_test->$new_attribute, $new_value);
      $this->assertEqual($entity->field_test[0]->$new_attribute, $new_value);
    }

    // Test sample item generation.
    $entity = EntityTest::create();
    $entity->field_test->generateSampleItems();
    $this->entityValidateAndSave($entity);
  }

}
