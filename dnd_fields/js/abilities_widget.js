/**
 * @file
 * Javascript for D&D Fields widgets.
 */

/**
 * Calculates ability score bonuses on edit.
 */
(function ($) {

  'use strict';

  Drupal.behaviors.dnd_fields_abilities_widget = {
    attach: function () {
      $("#edit-field-abilities-wrapper input").keyup(function () {
        var value = $(this).val();
        if (value) {
          if ($(this).val().match('^([0-9]{0,2})$')) {
            var bonus = Math.floor((value - 10) / 2);
            if (bonus > 0) {
              bonus = '+'.concat(bonus);
            }
            $(this).next('.field-suffix').children('span').text(bonus);
          }
          else {
            $(this).val(value.slice(0,-1));
          }
        }
        else {
          $(this).next('.field-suffix').children('span').text('');
        }
      }).keyup();
    }
  };
})(jQuery);
